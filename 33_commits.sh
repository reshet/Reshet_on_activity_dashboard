#!/bin/bash

CURRENT_DATE=`date +%d.%m.%Y`
README_STRING=`grep $CURRENT_DATE README.md`

if [ -z "$README_STRING" ]
then
	NEW_README=`grep '\[\ \]' README.md`
	if [ -z "$NEW_README" ]
	then
		echo "Generating new README with new dates"

		BUILDDIR=$(mktemp -d)
		git clone git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}.git $BUILDDIR
		cd $BUILDDIR

		git branch --no-track $CURRENT_DATE refs/heads/master
		git checkout $CURRENT_DATE
		ARRAY='nnnnnnnnnnnnnnyyyyyyyynnynnnynnyynnynnynynnyynnnynnnnnnnnnnyyynnnynynynnynynynnynynynnnynnnnnnnnnnnnnynnynnynynynnynynynnynynynnnnnynnnnnnnnyyyyyyynnnynnnnnynnnnnnynnnnnnnyyyynnnnnnnnnnyyynnnynynynnynynynnynynynnnynnnnnnnnnnnnynnnnyyyyyynnnynnnynnnnnnynnnnnyn'
		CURRENT_MONTH=''
		ITERATION_DAY=`date --utc --date $(date +%Y-%m-%d -d "Next Sunday") +%s`
		echo 'This is a fake project created to draw "Reshet" on my activity dashboard https://gitlab.com/reshet' > README.md
		echo >> README.md
		echo 'It requires 30+ push events on specified dates:' >> README.md
		while read -n1 create_date
		do
			if [[ "$create_date" == "y" ]]
			then
				if [[ $((10#$CURRENT_MONTH)) -ne $((10#$(date --utc --date @$ITERATION_DAY +%m))) ]]
				then
					CURRENT_MONTH=`date --utc --date @$ITERATION_DAY +%m`
					date --utc --date @$ITERATION_DAY +"%n**%B, %Y**" >> README.md
				fi
				date --utc --date @$ITERATION_DAY +"* [ ] %d.%m.%Y" >> README.md
			fi
			ITERATION_DAY=$((ITERATION_DAY + 86400))
		done < <(echo -n "${ARRAY}")
		git add README.md
		git commit -S -m "Update README.md with new dates"
		git checkout --ignore-other-worktrees develop
		git merge --no-ff -m "Merge README changes to develop" $CURRENT_DATE
		git push
		git checkout --ignore-other-worktrees staging
		git merge --no-ff -m "Merge README changes to staging" $CURRENT_DATE
		git push
		git checkout --ignore-other-worktrees master
		git merge --no-ff -m "Merge README changes to master" $CURRENT_DATE
		git push
		git branch -D $CURRENT_DATE
		cd
		rm -rf $BUILDDIR
	else
		echo "Nothing to do";
	fi
else
	BUILDDIR=$(mktemp -d)
	git clone git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}.git $BUILDDIR
	cd $BUILDDIR

	git branch --no-track $CURRENT_DATE refs/heads/master
	git checkout $CURRENT_DATE
	touch $CURRENT_DATE
	git add $CURRENT_DATE
	git commit -S -m "Added file for $CURRENT_DATE"

	git checkout --ignore-other-worktrees develop
	git merge --no-ff -m "Merge $CURRENT_DATE to develop" $CURRENT_DATE
	git push
	git checkout --ignore-other-worktrees staging
	git merge --no-ff -m "Merge $CURRENT_DATE to staging" $CURRENT_DATE
	git push
	git checkout --ignore-other-worktrees master
	git merge --no-ff -m "Merge $CURRENT_DATE to master" $CURRENT_DATE
	git push
	git branch -D $CURRENT_DATE

	n=0

	while IFS='' read -r line
	do
		n=`expr $n + 1`
		git branch --no-track $CURRENT_DATE refs/heads/master
		git checkout $CURRENT_DATE
		echo $line >> $CURRENT_DATE
		git add $CURRENT_DATE
		git commit -S -m "Added line $n to file $CURRENT_DATE"
		git checkout --ignore-other-worktrees develop
		git merge --no-ff -m "Merge '$CURRENT_DATE line $n' to develop" $CURRENT_DATE
		git push
		git checkout --ignore-other-worktrees staging
		git merge --no-ff -m "Merge '$CURRENT_DATE line $n' to staging" $CURRENT_DATE
		git push
		git checkout --ignore-other-worktrees master
		git merge --no-ff -m "Merge '$CURRENT_DATE line $n' to master" $CURRENT_DATE
		git push
		git branch -D $CURRENT_DATE
	done < 33_commits.txt
	n=`expr $n + 1`
	git branch --no-track $CURRENT_DATE refs/heads/master
	git checkout $CURRENT_DATE
	echo $CURRENT_DATE >> $CURRENT_DATE
	git add $CURRENT_DATE
	git commit -S -m "Added line $n to file $CURRENT_DATE"
	git checkout --ignore-other-worktrees develop
	git merge --no-ff -m "Merge '$CURRENT_DATE line $n' to develop" $CURRENT_DATE
	git push
	git checkout --ignore-other-worktrees staging
	git merge --no-ff -m "Merge '$CURRENT_DATE line $n' to staging" $CURRENT_DATE
	git push
	git checkout --ignore-other-worktrees master
	git merge --no-ff -m "Merge '$CURRENT_DATE line $n' to master" $CURRENT_DATE
	git push
	git branch -D $CURRENT_DATE
	n=`expr $n + 1`
	git branch --no-track $CURRENT_DATE refs/heads/master
	git checkout $CURRENT_DATE
	rm $CURRENT_DATE
	git rm $CURRENT_DATE
	sed -i -E "/$CURRENT_DATE/ s/\[\ \]/[x]/g" README.md
	git add README.md
	git commit -S -m "Current date $CURRENT_DATE checked in README.md"
	git checkout --ignore-other-worktrees develop
	git merge --no-ff -m "Merge README changes to develop" $CURRENT_DATE
	git push
	git checkout --ignore-other-worktrees staging
	git merge --no-ff -m "Merge README changes to staging" $CURRENT_DATE
	git push
	git checkout --ignore-other-worktrees master
	git merge --no-ff -m "Merge README changes to master" $CURRENT_DATE
	git push
	git branch -D $CURRENT_DATE
	n=`expr $n + 1`
	p=`expr $n \* 3`
	echo "$n lines commited, number of push events should be at least $p"
	cd
	rm -rf $BUILDDIR
fi
